InfluxDB::Rails.configure do |config|
  config.influxdb_database = "test"
  config.influxdb_username = "root"
  config.influxdb_password = "root"
  config.influxdb_hosts    = ["influx"]
  config.influxdb_port     = 8086

  # config.retry = false
  # config.async = false
  # config.open_timeout = 5
  # config.read_timeout = 30
  # config.max_delay = 300
  # config.time_precision = 'ms'

  config.series_name_for_controller_runtimes = "rails.schoeffm.controller".freeze
  config.series_name_for_view_runtimes       = "rails.schoeffm.view".freeze
  config.series_name_for_db_runtimes         = "rails.schoeffm.db".freeze
  # config.series_name_for_exceptions          = "rails.exceptions".freeze
  # config.series_name_for_instrumentation     = "instrumentation".freeze


  # Set the application name to something meaningful, by default we
  # infer the app name from the Rails.application class name.
  # config.application_name = Rails.application.class.parent_name
  config.application_name = "schoeffm-lobster"
end