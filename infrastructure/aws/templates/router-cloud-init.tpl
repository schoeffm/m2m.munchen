#cloud-config

package_upgrade: true

packages:
  - awscli
  - build-essential

runcmd:
- wget -qO- https://deb.nodesource.com/setup_8.x | sudo -E bash -
- sudo apt-get install -y nodejs
- AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${lobsters_bucket}/${router_code}" /home/ubuntu/${router_code}
- mkdir /home/ubuntu/m2m.router
- chown -R ubuntu /home/ubuntu/m2m.router
- su -l -c "tar xzf /home/ubuntu/m2m.router.tgz" ubuntu
- su -l -c "export ZIPKIN_BASE_URL=http://${tracing_host}:9411 && export LOBSTERS_URL=${lobster_url} && export MODLOG_URL=${modlog_url} && cd m2m.router && node index.js" ubuntu

