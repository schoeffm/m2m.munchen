#cloud-config
write_files:
-   encoding: b64
    path:  /var/lib/application-production.properties
    content: "${application_properties_content}"
    permissions: '0644'
-   encoding: b64
    path:  /etc/telegraf/telegraf.d/telegraf.conf
    content: "${telegraf_config_content}"
    permissions: '0644'

package_upgrade: true

packages:
  - awscli
  - openjdk-8-jdk

runcmd:
- wget https://dl.influxdata.com/telegraf/releases/telegraf_1.6.3-1_amd64.deb
- sudo dpkg -i telegraf_1.6.3-1_amd64.deb
- AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${lobsters_bucket}/${request_code}" /home/ubuntu/${request_code}
- chown -R ubuntu /home/ubuntu/m2m.request.jar
- su -l -c "cp -r /var/lib/application-production.properties /home/ubuntu/" ubuntu
- su -l -c "java -jar m2m.request.jar --spring.profiles.active=production" ubuntu
